package com.adderallnighters.bottomnavbar;

import com.adderallnighters.bottomnavbar.R;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;

public class NavigationBarFragment extends Fragment {

	private static final String TAG = NavigationBarFragment.class
			.getSimpleName();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_navigation_bar, parent,false);
		NavigationButtonChangeListener listener = new NavigationButtonChangeListener();

		RadioButton mCrewButton, mNewSpotButton, mFeedButton, mSettingsButton;

		mCrewButton = (RadioButton) view.findViewById(R.id.button_crew);
		mCrewButton.setOnCheckedChangeListener(listener);

		mNewSpotButton = (RadioButton) view.findViewById(R.id.button_new_spot);
		mNewSpotButton.setOnCheckedChangeListener(listener);

		mFeedButton = (RadioButton) view.findViewById(R.id.button_feed);
		mFeedButton.setOnCheckedChangeListener(listener);
		mFeedButton.setChecked(true);
		
		mSettingsButton = (RadioButton) view.findViewById(R.id.button_settings);
		mSettingsButton.setOnCheckedChangeListener(listener);

		return view;
	}

	private class NavigationButtonChangeListener implements
			OnCheckedChangeListener {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			if (isChecked) {
				Log.d(TAG, buttonView.getText() + " is checked! Winning!");
			}

		}

	}

}
