package com.adderallnighters.bottomnavbar;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import com.adderallnighters.bottomnavbar.R;

public class MainActivity extends FragmentActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		FragmentManager fragMan = getSupportFragmentManager();
		Fragment frag = fragMan.findFragmentById(R.id.fragmentLayout);
		if(frag == null){
			frag = new NavigationBarFragment();
			fragMan.beginTransaction().add(R.id.fragmentContainer, frag).commit();
		}
	}
	
	@Override 
	public void onSaveInstanceState(Bundle savedInstanceState){
		super.onSaveInstanceState(savedInstanceState);
	}
}